package fp.daw.examen1ev;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Dime un a�o");
		int a�o = in.nextInt();
		esBisiesto(a�o);
		in.close();

	}

	public static void esBisiesto(int a�o) {
		if ((a�o % 4 == 0) && ((a�o % 100 != 0) || (a�o % 400 == 0)))

			System.out.println("El a�o es bisiesto");
		else
			System.out.println("El a�o no es bisiesto");
		/*
		 * do { System.out.println("El a�o es bisiesto"); break; }while((a�o % 4 == 0)
		 * && ((a�o % 100 != 0) || (a�o % 400 == 0)));
		 * 
		 * do { System.out.println("El a�o no es bisiesto"); break; }while((a�o % 4 !=
		 * 0) && ((a�o % 100 == 0) || (a�o % 400 != 0)));
		 */
	}

}
